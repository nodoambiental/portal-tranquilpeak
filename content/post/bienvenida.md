---
title: "¿Qué es Nodo Ambiental?"
# author: "beta"
date: "2019-02-11"
categories: ["organizaciones"]
tags: ["información", "nexo", "ONG", "OSC", "organización", "articulación"]
thumbnailImage: /img/diqueperro.jpg
thumbnailImagePosition: top
---

# _Información y nexos para la preservación del ambiente, el desarrollo sostenible y el bienestar social_

**_Nodo Ambiental_** es una organización que colabora con el manejo de información y con redes de cooperación para la preservación del ambiente, el desarrollo sostenible y el bienestar social.

<!--more-->

**_Nodo Ambiental_** es una Organización de la Sociedad Civil (OSC), sin fines de lucro, no gubernamental (ONG), dirigida a otras organizaciones, escuelas y profesionales.


# _Buscamos_

- 1 Colaborar en el desarrollo y ejecución de proyectos de pertinencia social y ambiental.

- 2 Difundir proyectos de salud ambiental, incluyendo aspectos sociales, educativos y lúdicos.

- 3 Desarrollar repositorios de información verificable.


# _Nuestra Visión_

Los problemas de salud humana y de bienestar social están embebidos en problemas de conservación de la naturaleza y del manejo de recursos naturales. La salud humana y ambiental, la salud de los servicios que brinda la naturaleza -recursos naturales comunes o compartidos (aire, agua, suelo) que nos llegan como beneficios espontáneos de la naturaleza-, son afectados por actividades humanas. 

La suma de pequeños impactos individuales humanos tiene efectos muy importantes sobre la salud ambiental y humana, y finalmente, sobre el bienestar social.

<!--more-->

Es por ello que la conjunción de mecanismos de articulación, desarrollo humano, desarrollo tecnológico, herramientas de cooperación entre organizaciones, espacios educativos, artísticos y multimedios, ayudan a encontrar soluciones prácticas en _entornos donde el tiempo es limitado_.

Además, la verificación de la información difundida es clave para establecer bases sólidas para la realización y alcance de metas.

El desarrollo de nodos o sistemas de cooperación y articulación es una herramienta sólida para ayudar a que los proyectos de educación ambiental, conservación de la naturaleza y salud humana y ambiental, logren sus metas. Entre ellas, la reducción del impacto humano sobre el ambiente en que vivimos y convivimos.


# _Nuestra Misión_

Desarrollar sistemas de cooperación entre Organizaciones de la Sociedad Civil (ONG), escuelas y profesionales, para la verificación de información y para la difusión, desarrollo y ejecución de proyectos de pertinencia ambiental y social.

![cuesta](/img/cuestachilcapasto.jpg)

# _Cómo trabajamos_

Proponemos un esquema de trabajo mutuamente verificable. A un primer proceso de contacto y formulación de objetivos, prosigue un esquema de interacción para el alcance de metas.


# _Lemas_
_La conservación de la naturaleza es nuestra salud y bienestar social_

# _Nodo Ambiental_
Estamos en período de prueba. Escriba a correonodoambiental@gmail.com si considera colaborar con nosotros. 

Le invitamos a poner a prueba nuestros servicios.

La sección _Términos y condiciones_ incluye los términos y condiciones del uso de datos.


# _Desarrollo web_

Portal desarrollado mediante blogdown [https://bookdown.org/yihui/blogdown/], RStudio Version 1.1.456 [https://www.rstudio.com/], R Version 3.5.2 [https://www.r-project.org/], tranquilpeak para Hugo [https://github.com/kakawait/hugo-tranquilpeak-theme], repositorios GitHub [https://github.com/nodoambiental/] y GitLab [https://gitlab.com/nodoambiental/], dominio vía donweb [https://donweb.com/es-ar/], hosting y certificados en netlify [https://app.netlify.com/].


# _Términos y condiciones_

Información para difusión disponible bajo licencia MIT [https://opensource.org/licenses/MIT] y Creative Commons [https://creativecommons.org/].


# _Directrices de privacidad_
  _nodoambiental.org_


**Cómo registramos, usamos, y compartimos información personal**
La información personal que usted brinda a través del registro en nuestro portal, o la que nos envía para la realización de servicios o actividades y relaciones específicas, queda en un repositorio seguro de nodoambiental [https://gitlab.com/nodoambiental].


No compartimos información personal de su registro en nodoambiental sin su consentimiento explícito (correo electrónico o formulario).


En el caso de que usted haya firmado con su nombre entrevistas, o haga comentarios en foros, o participe en la generación de contenidos vinculados a tareas de nodoambiental, su nombre, localidad geográfica y grupo de pertenencia, lo consideramos información pública, misma que usted podrá visualizar en los contenidos que nodoambiental haga públicos o comparta con usted.


Siempre podrá retirar información personal a resguardo en los repositorios, o restringir el uso de información que explícitamente nos comunique, con excepción de la información de su nombre, localidad, o grupo de pertinencia y actividades relacionadas a las tareas de nodoambiental que se consideren públicas.


La información de identificación personal (tales como documentos, pasaportes, u otra información de referencia fiscal o legal) que usted envía para nuestros registros será clasificada a resguardo seguro, en el caso de que usted voluntariamente nos envíe tal información.


La información sobre domicilio postal o de residencia no será pública. Dicha información será accedida solamente por miembros del consejo de nodoambiental, o sólo para usos específicos vinculados con eventos donde dicha información debe figurar y ser compartida con restricciones específicas, nunca de dominio público. 


Para la correspondencia o interacción directa en la web (chat, foros) usaremos su primer nombre, el cual usted podrá ver siempre.


**Cómo protegemos información personal**
En _nodoambiental_ ejecutamos y mantenemos medidas de seguridad y prácticas apropiadas para el registro, repositorio (almacenamiento) y procesamiento para la protección contra accesos no autorizados, alteraciones de información, revelación o destrucción de su información personal, nombre de usuario, contraseña, información de transacciones web y datos almacenados en nuestro sitio. 


**Cambios en estas directrices de privacidad**
Nodoambiental conserva el criterio de actualizar estas directrices de privacidad en cualquier momento. Cuando hacemos esto, publicamos la fecha de modificación al pie de esta página.


El reconocimiento y aceptación de las actualizaciones de estas directrices de privacidad queda bajo su responsabilidad, por lo que le sugerimos revisarlos periódicamente y estar al tanto de los cambios que realizamos. 


Le enviaremos actualizaciones sobre estos cambios y otros anuncios vía correo electrónico. Usted puede dejar de estar suscripto de nuestra lista de correos en cualquier momento que lo desee.


Si no desea recibir mensajes, por favor, indíquelo a nuestro correo: [correonodoambiental@gmail.com]

<!-- toc -->
