---
date: "2019-02-10"
slug: para-profesionales
thumbnailImage: /img/florquimilo.jpg
thumbnailImagePosition: left
categories: ["profesionales"]
tags: ["información","integración", "multidisciplina", "articulación","docencia","educación ambiental","participación","equipo"]
title: Para profesionales
---

### _¿Te interesa discutir temas y desarrollar proyectos?_

<!--more-->

### Convocamos profesionales de todas las áreas

![algarrobo](/img/purmaalgarrobo.jpg)

