---
date: "2019-02-10"
slug: para-organizaciones-y-escuelas
thumbnailImage: /img/fuego2.jpg
thumbnailImagePosition: left
categories: ["organizaciones"]
tags: ["información","nexo","ONG","OSC","asociación civil","fundación","articulación","escuela","educación ambiental","servicios"]
title: Para organizaciones y escuelas
---

### _A tu organización o escuela le hace falta..._

<!--more-->

### - ¿tiempo o ayuda para desarrollar o gestionar un proyecto?

### - ¿un portal web?

### - ¿difundir un tema?

### - ¿información sobre un problema o tema?

### - ¿verificar información publicada ("fake news")?

### - ¿llevar a cabo una encuesta?

### - ¿desarrollar un repositorio digital de información verificable?


Ofrecemos estos servicios a organizaciones y escuelas sin recursos, de manera gratuita.


Ofrecemos servicios con cargo a organizaciones gubernamentales y empresas. Los fondos obtenidos se destinan a los proyectos de Nodo Ambiental.


![fuego](/img/fuego1.jpg)
