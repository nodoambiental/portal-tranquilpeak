---
date: "2019-02-10"
slug: para-voluntarios
categories: ["voluntariado"]
tags: ["información", "programación", "proyectos", "organización","aprendizaje"]
thumbnailImage: /img/moconamariposa.jpg
thumbnailImagePosition: left
title: Para voluntarios
---

### _¿Te interesa colaborar con tu tiempo?_

<!--more-->

Construimos un programa de voluntariado para fortalecer el desarrollo de proyectos y aprender mutuamente de diversas experiencias.

![passiflora](/img/passiflora.jpg)



