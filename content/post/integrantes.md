---
date: "2019-02-08"
categories: ["integrantes"]
tags: ["ONG","OSC","organización","articulación","equipo","roles","miembros","administración"]
thumbnailImage: /img/vallecardones.jpg
thumbnailImagePosition: left
title: Integrantes
---

### _Nodo Ambiental se forma como un equipo multidisciplinario de profesionales, docentes y técnicos_

<!--more-->

### Quiénes somos

*Laura González*, Antropóloga Social ![laura](/img/laura-cut.png)

*Mariano Ordano*, Biólogo, Doctor en Ciencias en Ecología y Manejo de Recursos Naturales ![mariano](/img/mariano-cut.png)

*Facundo Bernacki*, Licenciado en Ciencias Biológicas, Docente

*Renzo Bernacki*, Licenciado en Ciencias Biológicas, Docente

*Cecilia Ordano*, Profesora de Historia, especialista en recursos humanos

![cardones](/img/vallecardones.jpg)
